(* ::Package:: *)

BeginPackage["wlmonad`"];


bind::usage = "bind[m] 

The definition of bind[m] is supplied for each monad m by the user and it should
implement the bind operator:
   bind[m][ma , aTOmb]
where:
   ma
matches:
   pattern[m]
and aTOmb is a function:
   a -> mb
where:
   mb
satisfies:
   pattern[m]
";
return::usage = "return[m] 

Return function for monad m. Defined by the user:
   return[m][x]
evaluates to an expression that matches:
   pattern[m]
";
pattern::usage="pattern[m]
  
Defined by the user. Evaluates to a pattern that matches any 
expression being a monad of type m.
";


do ::usage = "do[m][
   a,
   b,
   ...
   r
]

Do notation implemented for monad m. The do expression is built up from a , b , ... in the form:
   x\[LeftArrow]y
or:
   y
where y is of type:
   m b
and x is a unique variable name. The final expression r is of type:
   m c
";


fishC::usage = "fishC[m][aTOmb][bTOmc]

Implements ma >>= aTOmb :: a -> mc for monad m.
";


fish::usage = "aTOmb~fish[m]~bTOmc

Implements ma >>= aTOmb for monad m.
";


Begin["`Private`"];


fst::usage = "fst[a\[LeftArrow]b] 

Evaluates to a.
";
snd::usage = "snd[a\[LeftArrow]b] 

Evaluates to b.
";
chk::usage = "chk[m][mx]

Checks if mx matches pattern[mx]. If it does returns mx.
";
bnd::usage = "bnd[m][ma , aTOmb]

Evaluates to:
   bind[m][ma , aTOmb]
if ma matches:
   pattern[m]
and the head of aTOmb is Function.
";


fst[LeftArrow[a_ , b_]]:=a;
snd[LeftArrow[a_ , b_]]:=b;
chk[m_][mx:Except[bnd[_][_ , _]|chk[_][_]]]:= 
	If[
		MatchQ[mx , pattern[m]] , 
		mx , 
		Throw["Expression: "<>ToString[mx]<>" does not match pattern["<>ToString[m]<>"]"]
	];
bnd[m_][ma_ , aTOmb_]:=
	bind[m][ma , aTOmb]/;MatchQ[ma , pattern[m]]&&(Head[aTOmb]==Function);


do[m_][x___ , y_ , r:Except[_LeftArrow]]:= 
  If[MatchQ[y , LeftArrow[_ , _]] , 
     do[m][x , chk[m][bnd[m][snd[y] , Function[Evaluate[fst[y]] , r]]]],
     do[m][x , chk[m][bnd[m][y , Function[Evaluate[Unique["nonExistantVariable"]] , r]]]]
  ];
do[m_][mx_]:=chk[m][mx];


fishC[m_][aTOmb_][bTOmc_]:=Function[a , With[{mb = aTOmb[a]},chk[m][bnd[m][mb , Function[b , bTOmc[b]]]]]];


SetAttributes[fish , {Flat , OneIdentity}];
fish[m_][action1_ , action2_]:= fishC[m][action1][action2];


End[];


EndPackage[];
