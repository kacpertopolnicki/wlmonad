# wlmonad

Monads and "do" notation in the Wolfram Language!

More information on this project is available:

- [here](https://kacpertopol.github.io/myblog/2021-01-31_gen_light.html),
- [here](https://kacpertopol.github.io/myblog/2021-02-04_gen_light.html), 
- and [here](https://kacpertopol.github.io/myblog/2021-02-08_gen_light.html).

These posts are recommended reading :-) 

# `wlmonad.wl`

This is the main package that introduces 'do' notation into the Wolfram Language.

# `....nb`

These notebooks contain examples of using the package. The Hanoi Tower example
is described in the blog posts and may be a good starting point.